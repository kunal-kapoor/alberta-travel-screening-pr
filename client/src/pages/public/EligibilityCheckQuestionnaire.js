import React from 'react';
import Grid from '@material-ui/core/Grid';
import { Formik, Form as FormikForm } from 'formik';
import { useTranslation } from "react-i18next";

import { EnrollmentFormSchema, EligibilityCheckValidationSchema } from '../../constants';
import { FastField } from 'formik';

import { Card, Button, InputFieldLabel } from '../../components/generic';
import { FocusError } from '../../components/generic';
import { Questions } from '../../constants';
import { RenderRadioGroup } from '../../components/fields';
import { Box, Typography } from '@material-ui/core';
import { EligibilityQuestions } from '../../constants/eligibilityQuestions';


export const EligibilityCheckQuestionnaire = ({ onSubmit, initialValues, isDisabled, isFetching }) => {
  const { t } = useTranslation();

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={EligibilityCheckValidationSchema}
      onSubmit={onSubmit}
    >
      <FormikForm>
        <FocusError />
        <Card title={t('Pilot Eligibility Check')}>
            <Grid container spacing={2}>
                {EligibilityQuestions.map(({question, name}) => (
                    <Grid item xs={12} key={name}>
                        <InputFieldLabel label={question}></InputFieldLabel>
                        <FastField
                            name={name}
                            label=""
                            component={RenderRadioGroup}
                            disabled={isDisabled}
                            options={Questions}
                        />
                    </Grid>
                ))}

                {(!isDisabled) && (
                <Grid item xs={12} sm={7} md={6} lg={4} xl={2}>
                    <Box ml={[1, 0]} mr={[1, 0]}>
                        <Button
                            text="Continue"
                            type="submit"
                            loading={isFetching}
                            />
                    </Box>
                </Grid>
                )}
            </Grid>            
        </Card>
      </FormikForm>
    </Formik>
  );
};