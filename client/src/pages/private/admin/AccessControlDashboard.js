import React from 'react';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import MuiTable from '@material-ui/core/Table';
import { Formik, Form as FormikForm, FastField, Field } from 'formik';

import { useAccessControlLookup, useDefaultReportDashboards } from '../../../hooks';
import { Page, Button, Card } from '../../../components/generic';
import { RenderChipSelectField, RenderTextField } from '../../../components/fields';
import { AccessControlValidationSchema } from '../../../constants';

export default () => {
  const {
    isFetching,
    tableData,
    updateAccessControl
  } = useAccessControlLookup();

  const { availableReports } = useDefaultReportDashboards();

  return (
    <Page hideFooter>

      {/** Blue Banner */}
      <Box pt={[3, 4, 6]} pb={[3, 4, 6]} bgcolor="secondary.main" color="common.white">
        <Container maxWidth="md">
          <Typography variant="h1">Access Control</Typography>
        </Container>
      </Box>

      {/** White Banner */}
      <Box pt={[3, 3]} pb={[3, 3]}>
        <Container maxWidth="md">
          <Grid container justify={"space-between"} alignItems="center" spacing={2}>
            <Grid item xs={12} sm={8} md={12}>
              <Formik
                initialValues={{
                  'reportName': [],
                  'agentEmail': ''
                }}
                validationSchema={AccessControlValidationSchema}
                onSubmit={(values, {resetForm} ) => updateAccessControl(values,false, resetForm)}
              >
                <FormikForm>
                  <Grid container justify="space-between" alignItems="center" spacing={1}>
                    <Grid item xs={12} sm={6} md={4}>
                      <FastField
                        name="agentEmail"
                        label="Agent Email*"
                        component={RenderTextField}
                        placeholder="Required"
                      />
                    </Grid>
                    <Grid item xs={12} md={5}>
                      <Field
                        label="Grant*"
                        name='reportName'
                        placeholder="Select Grant..."
                        component={RenderChipSelectField}
                        options={availableReports}
                      />
                    </Grid>
                    <Grid item xs={12} md={1}>
                      <label>&nbsp;</label>
                      <Button
                        disabled={isFetching}
                        type="submit"
                        text="Add"
                      />
                    </Grid>
                  </Grid>
                </FormikForm>
              </Formik>
            </Grid>
          </Grid>
        </Container>
      </Box>

      <Box py={3}>
        <Container maxWidth="md">
          <Card style={{ overflowX: 'auto' }} noPadding>
            <MuiTable style={{ tableLayout: 'fixed' }} size="small">
              <TableHead>
                <TableRow>
                  <TableCell align="center">Agent Email</TableCell>
                  <TableCell align="right">Grants</TableCell>
                  <TableCell></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {tableData.rows.map(eachRow => (
                  <TableRow key={eachRow.agentEmail}>
                    <TableCell scope="row" colSpan="3">
                      <Formik
                        initialValues={eachRow}
                        onSubmit={values => updateAccessControl(values,true)}
                      >
                        <FormikForm>
                          <Grid container justify="space-between" alignItems="center" spacing={1}>
                            <Grid item xs={12} sm={6} md={4}>
                              <FastField
                                name="agentEmail"
                                component={RenderTextField}
                                InputProps={{
                                  readOnly: true,
                                }}
                              />
                            </Grid>
                            <Grid item xs={12} md={5}>
                              <Field
                                name='reportName'
                                placeholder="Select Grant..."
                                component={RenderChipSelectField}
                                options={availableReports}
                              />
                            </Grid>
                            <Grid item xs={12} md={1}>
                              <Button
                                disabled={isFetching}
                                type="submit"
                                text="Update"
                              />
                            </Grid>
                          </Grid>
                        </FormikForm>
                      </Formik>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </MuiTable>
          </Card>
        </Container>
      </Box>
    </Page>
  );
};
