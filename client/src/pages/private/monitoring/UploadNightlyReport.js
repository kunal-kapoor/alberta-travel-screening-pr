import React, { useEffect, useState } from 'react';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import * as csv from 'csvtojson';

import { Page, Button } from '../../../components/generic';
import { RenderFileDropzone } from '../../../components/fields/RenderFileDropzone';
import { useToast } from '../../../hooks';
import { AxiosPrivate } from '../../../utils';
import Upload from '../../../assets/images/upload.png';


export default () => {
  const { openToast } = useToast();
  const [fileData, setFileData] = useState();
  const [isFileUploaded, setFileUploaded] = useState(false);
  const [isFileValid, setFileValidity] = useState(undefined);

  const handleFileDrop = (file) => {
    setFileData(undefined)
    setFileUploaded(false)
    setFileValidity(undefined)

    if (file.length) {
      const reader = new FileReader();
      reader.onload = function(e) {
        const contents = e.target.result;
        csv({noheader: true}).fromString(contents).then((json) => {
          // Remove first index - headers
          json.shift()
          let dto = [];
          json.map((element) => {
            dto.push({confirmationNumber: element.field1, testedAt: element.field2});
          })
          if (dto.length) {
            setFileData(dto);
            setFileUploaded(true);
            setFileValidity(true)
          } else setFileValidity(false)

        });
      };
      
      reader.readAsText(file[0]);
    }
  }

  const handleUploadRequest = async () => {
    if (fileData) {
      try {
          const uploadResponse = await AxiosPrivate.post('/api/v2/rtop-admin/test-results', {results: fileData});
          if(uploadResponse.errors?.length) {
            openToast({ status: 'error', message: `${uploadResponse.errors[0].confirmationNumber} ${uploadResponse.errors[0].failureReason}` || "Failed to upload nightly report."});
          }
      } catch(e) {
        openToast({ status: 'error', message: e.message || "Failed to upload nightly report."});
      }
    }
  }

  useEffect(() => {
    document.title = 'Alberta COVID-19 Border Testing Pilot Program - Testing Report Upload'
  }, [])

  return (
    <Page hideFooter>
      {/** Blue Banner */}
      <Box pt={[3, 4, 6]} pb={[3, 4, 6]} bgcolor="secondary.main" color="common.white">
        <Container maxWidth="md">
          <Typography variant="h1">Upload Testing Report</Typography>
        </Container>
      </Box>

      {/** White Banner */}
      <Box
        mt={4} mb={4}
      >
        <Container maxWidth="md" m>
          <Grid container justify="space-between" alignItems="center" spacing={2}>
            <Grid item xs={12} sm={12}>
              <RenderFileDropzone 
                fileUploaded={isFileUploaded}
                displayValidity={isFileValid}
                uploadCallbackHandler={(file) => handleFileDrop(file)} 
                icon={Upload} 
              />
            </Grid>
            <Grid item xs={12} sm={3  }>
              <Button 
                text="Submit Report"
                onClick={handleUploadRequest}
                disabled={!isFileUploaded}
              />
            </Grid>
          </Grid>
        </Container>
      </Box>
    </Page>
  )
}