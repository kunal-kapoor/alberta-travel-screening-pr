import React from 'react';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { Formik, Form as FormikForm, FastField } from 'formik';

import { useReportLookup } from '../../../hooks';

import { Page, Table, Button } from '../../../components/generic';
import { RenderSelectField, RenderDateField } from '../../../components/fields';
import moment from "moment";
import { ReportValidationSchema } from '../../../constants/report-validation';

export default () => {
    const {
        tableData,
        isFetching,
        hasExecuted,
        reportOptions,
        executeReport,
      } = useReportLookup();
    
    return (
        <Page hideFooter>
    
          {/** Blue Banner */}
          <Box pt={[3, 4, 6]} pb={[3, 4, 6]} bgcolor="secondary.main" color="common.white">
            <Container maxWidth="md">
              <Typography variant="h1">Reports</Typography>
            </Container>
          </Box>
    
          {/** White Banner */}
          <Box pt={[3, 3]} pb={[3, 3]}>
            <Container maxWidth="md">
              <Grid container justify={hasExecuted? "space-between": "center"} alignItems="center" spacing={2}>
                <Grid item xs={12} sm={hasExecuted? 12: 8} md={hasExecuted? 12: 6}>
                  <Formik
                          validationSchema={ReportValidationSchema}
                          onSubmit={values => executeReport(values)}
                          initialValues={{
                            'report': '',
                            'fromDate': moment().subtract(7, 'd').startOf('day').format('YYYY/MM/DD'),
                            'toDate': moment().endOf('day').format('YYYY/MM/DD')
                          }}
                      >
                      <FormikForm>
                        <Grid container justify="space-between" alignItems="center" spacing={1}>
                          <Grid item xs={12} sm={hasExecuted? 6: null} md={hasExecuted? 4: null}>
                          { reportOptions.length > 0 && (
                            <FastField
                                label="Report*"
                                name="report"
                                placeholder="Select Report..."
                                component={RenderSelectField}
                                options={reportOptions}
                            />
                            )}
                          </Grid>
                          <Grid item xs={!hasExecuted? 12: null}>
                            <FastField
                                name="fromDate"
                                label="From Date* (YYYY/MM/DD)"
                                component={RenderDateField}
                                placeholder="Required"
                                disableFuture={false}
                            />
                          </Grid>
                          <Grid item xs={!hasExecuted? 12: null}>
                            <FastField
                                name="toDate"
                                label="To Date* (YYYY/MM/DD)"
                                component={RenderDateField}
                                placeholder="Required"
                                disableFuture={false}
                            />
                          </Grid>
                          <Grid item xs={!hasExecuted? 12: null}>
                            <h6></h6>
                            <Button
                                disabled={isFetching}
                                type="submit"
                                text="Submit"
                            />
                          </Grid>
                        </Grid>
                      </FormikForm>
                  </Formik>
                </Grid>
              </Grid>
            </Container>
          </Box>
    
          {/** Table */}
          <Box py={3}>
            <Container maxWidth="md">
              {hasExecuted &&<Table
                columns={tableData.columns}
                rows={tableData.rows}
                isLoading={isFetching}
              />}
            </Container>
          </Box>
        </Page>
      );
    };
