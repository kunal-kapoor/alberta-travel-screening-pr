import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import { FastField, useFormikContext, Field, FieldArray } from 'formik';
import { useTranslation } from "react-i18next";

import { Questions, Question, Gender, CitizenshipStatus, ExemptionType, ExemptOccupation, ExemptOccupationDetails, Countries, TravelReason, StayDuration } from '../../../constants';

import { Card } from '../../generic';
import { RenderTextField, RenderDateField, RenderRadioGroup, RenderDynamicField, RenderSelectField } from '../../fields';

export const TravelInformation = ({ isDisabled }) => {
  const { values } = useFormikContext();
  const { t } = useTranslation();

  return (
    <Card title={t('Travel Information')}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <FastField
            name="hasAdditionalTravellers"
            label="Are you travelling with additional members from your household?*"
            component={RenderRadioGroup}
            disabled={isDisabled}
            options={Questions}
          />
        </Grid>
        {values.hasAdditionalTravellers === Question.Yes && (
          <Grid item xs={12}>
            <FieldArray name="additionalTravellers">
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <FastField
                    name="additionalTravellers"
                    selectName="numberOfAdditionalTravellers"
                    component={RenderDynamicField}
                    label="Number of additional travellers?*"
                    disabled={isDisabled}
                    itemMin={1}
                    itemMax={10}
                    itemFields={[
                      { name: 'firstName', label: "First name*", placeholder: "Required", component: RenderTextField },
                      { name: 'lastName', label: "Last name*", placeholder: "Required", component: RenderTextField },
                      { name: 'dateOfBirth', label: "Date of birth* (YYYY/MM/DD)", placeholder: "Required", component: RenderDateField },
                      { name: 'gender', label: "Gender Identity*", placeholder: "Required", component: RenderSelectField, options:Gender },
                      { name: 'exemptionType', label: "Do you belong to exemption category?*", placeholder: "Required", component: RenderSelectField, options:ExemptionType, md: 12 },
                      {
                        showIf: idx => values.additionalTravellers[idx] && values.additionalTravellers[idx]['exemptionType'] === ExemptionType[0].value,
                        name: 'exemptOccupation', label: "Occupation type/category", placeholder: "", component: RenderSelectField, options: ExemptOccupation, md: 12
                      },
                      {
                        showIf: idx => values.additionalTravellers[idx]['exemptionType'] === ExemptionType[0].value && values.additionalTravellers[idx]['exemptOccupation'] in ExemptOccupationDetails && !!ExemptOccupationDetails[values.additionalTravellers[idx]['exemptOccupation']].length,
                        name: 'exemptOccupationDetails', label: "Occupation Details", placeholder: "", component: RenderSelectField, md: 12, dynamicOptions: idx => ExemptOccupationDetails[values.additionalTravellers[idx]['exemptOccupation']]
                      },
                      { name: 'citizenshipStatus', label: "What's your current Canadian citizenship status?*", placeholder: "Required", component: RenderSelectField, options:CitizenshipStatus, md: 12},
                      { name: 'seatNumber', label: "Seat number(s) for flight", placeholder: "", component: RenderTextField },
                      { name: 'reasonForTravel', label: "Reason for travel", placeholder: "", component: RenderSelectField, options:TravelReason },
                      { name: 'durationOfStay', label: "Expected duration of stay", placeholder: "", component: RenderSelectField, options: StayDuration },

                    ]}
                    itemsWrapper={(fields) => (
                      <Card title={t("Additional Traveller Information")} isTitleSmall={true}>
                        {!isDisabled && (
                          <Typography variant="body2" paragraph>
                            {t('For each traveller, please list their first name, last name and date of birth')}
                          </Typography>
                        )}
                        {fields}
                      </Card>
                    )}
                  />
                </Grid>
              </Grid>
            </FieldArray>

          </Grid>
        )}
        <Grid item xs={12}>
          <FastField
            name="hasTravellersOutsideHousehold"
            label="Are there people outside your household in your travelling party?*"
            component={RenderRadioGroup}
            disabled={isDisabled}
            options={Questions}
          />
        </Grid>
        {values.hasTravellersOutsideHousehold === Question.Yes && (
          <Grid item xs={12} sm={12}>
            <Field
              name="travellersOutsideHousehold"
              label="Please list the names of these individuals*"
              component={RenderTextField}
              placeholder="Required"
              disabled={isDisabled}
              multiline
              rows={3}
            />
          </Grid>
        )}

      </Grid>
    </Card>
  );
};