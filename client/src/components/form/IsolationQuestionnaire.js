import React, { Fragment, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { FastField, Field, useFormikContext } from 'formik';
import { useTranslation } from "react-i18next";

import { Vehicles, Places, Provinces, Questions, Question, Place, Vehicle, Province } from '../../constants';

import { Card, InfoBox, Button } from '../generic';
import { RenderRadioGroup, RenderTextField, RenderSelectField } from '../fields';
import { IconButton } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import { withStyles } from '@material-ui/core/styles';

export const IsolationQuestionnaire = ({ isDisabled, isEditing, canEdit, setIsEditing }) => {
  const { values } = useFormikContext();
  const { t } = useTranslation();

  const StyledEditButton = withStyles((theme) => ({
    root: {
      position: 'absolute',
      left: '0',
      top: '-8px',
      color: theme.palette.common.white,
      '&:hover': {
        color: theme.palette.common.lightGray,
      },  
    }
  }))(IconButton);
  
  const EditButton = (<StyledEditButton
      centerRipple={false}
      onClick={() => {
        setIsEditing(true);
      }}
    >
      <EditIcon />
    </StyledEditButton>);
  
  const Title = canEdit? (<span style={{display: 'flex', justifyContent: 'space-between'}}>
    <span>{t("Isolation Questionnaire")}</span><span style={{height: '24px', width: '24px', position: 'relative'}}>{EditButton}</span>
  </span>): t("Isolation Questionnaire");

  return (
    <Card title={Title}>
      <Grid container spacing={2}>

        {!isDisabled && (
          <Grid item xs={12}>
            <Box mb={1}>
              <Typography variant="body2">
                {t("Quarantine and isolation mean staying home, not attending work, school, social events or any other public gatherings. You are also prohibited from going outside in your neighborhood and from taking public transportation like buses")}
              </Typography>
            </Box>
            <Typography component="div" variant="body2">
              <ul>
                <li>
                  {t("When you or members of your household do not have symptoms, you are in quarantine and must watch for symptoms (cough, fever, shortness of breath, runny nose or sore throat)")}
                </li>
                <li>
                  {t("If you or members of your household develop COVID or COVID-like symptoms, you must stay in isolation and avoid close contact with people in your household who are ill and those who are well, especially seniors and people with chronic conditions or compromised immune systems")}
                </li>
              </ul>
            </Typography>
          </Grid>
        )}

        <Grid item xs={12}>
          <Field
            name="hasPlaceToStayForQuarantine"
            label="Do you have a place to stay for the required quarantine or isolation period?*"
            component={RenderRadioGroup}
            disabled={isDisabled && !isEditing}
            options={Questions}
          />
        </Grid>

        {values.hasPlaceToStayForQuarantine === Question.Yes && (
          <Fragment>
            <Grid item xs={12}>
              <Typography varient="body1">
                {t('What is the address where you will be quarantining or isolating?')}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={12}>
              <Field
                name="quarantineLocation.address"
                label="Address*"
                component={RenderTextField}
                placeholder="Required"
                disabled={isDisabled && !isEditing}
                multiline
                rows={3}
              />
            </Grid>
            <Grid container spacing={2} item>
              <Grid item xs={12} sm={6}>
                <Field
                  name="quarantineLocation.cityOrTown"
                  label="City or Town*"
                  component={RenderTextField}
                  placeholder="Required"
                  disabled={isDisabled && !isEditing}
                />
              </Grid>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Field
                name="quarantineLocation.provinceTerritory"
                label="Province/Territory*"
                component={RenderSelectField}
                disabled={isDisabled && !isEditing}
                options={Provinces}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Field
                name="quarantineLocation.postalCode"
                label="Postal Code"
                component={RenderTextField}
                disabled={isDisabled && !isEditing}
              />
            </Grid>
            {values.quarantineLocation.provinceTerritory === Province.Other && (
            <Grid item xs={12}>
              <Field
                name="quarantineLocation.provinceTerritoryDetails"
                label="If Other, Please specify the Province/Territory*"
                component={RenderTextField}
                disabled={isDisabled && !isEditing}
              />
            </Grid>
            )}
            <Grid item xs={12}>
              <Field
                name="quarantineLocation.phoneNumber"
                label="Phone Number at Isolation Location*"
                component={RenderTextField}
                placeholder="Required"
                disabled={isDisabled && !isEditing}
              />
            </Grid>
            <Grid item xs={12}>
              <Field
                name="quarantineLocation.typeOfPlace"
                label="What type of a place is it?*"
                component={RenderSelectField}
                disabled={isDisabled && !isEditing}
                options={Places}
              />
            </Grid>
            {values.quarantineLocation.typeOfPlace === Place.Other && (
              <Grid item xs={12}>
                <Field
                  name="quarantineLocation.typeOfPlaceDetails"
                  label="Please provide further details*"
                  placeholder="Required"
                  component={RenderTextField}
                  disabled={isDisabled && !isEditing}
                />
              </Grid>
            )}
            <Grid item xs={12}>
              <Field
                name="quarantineLocation.howToGetToPlace"
                label="How will you get to this location?*"
                component={RenderSelectField}
                disabled={isDisabled && !isEditing}
                options={Vehicles}
              />
            </Grid>
            {values.quarantineLocation.howToGetToPlace === Vehicle.Other && (
              <Grid item xs={12}>
                <Field
                  name="quarantineLocation.howToGetToPlaceDetails"
                  label="Please provide further details*"
                  placeholder="Required"
                  component={RenderTextField}
                  disabled={isDisabled && !isEditing}
                />
              </Grid>
            )}
            <Grid item xs={12}>
              <Field
                name="quarantineLocation.otherPeopleResiding"
                label="Are there people residing at that location who are not travelling with you today?*"
                component={RenderRadioGroup}
                disabled={isDisabled && !isEditing}
                options={Questions}
              />
            </Grid>

            <Grid item xs={12}>
              <Field
                name="quarantineLocation.doesVulnerablePersonLiveThere"
                label="Does a vulnerable person also live there? For example, someone who is over the age of 65 or who has heart disease, high blood pressure, asthma or other lung disease, diabetes, cancer, or underlying health conditions?*"
                component={RenderRadioGroup}
                disabled={isDisabled && !isEditing}
                options={Questions}
              />
            </Grid>
          </Fragment>
        )}

        <Grid item xs={12}>
          <FastField
            name="isAbleToMakeNecessaryArrangements"
            label="Are you able to make the necessary arrangements during the applicable quarantine or isolation period for assistance to meet your basic needs? (e.g. food, medication, child care, cleaning supplies, pet care)?*"
            component={RenderRadioGroup}
            disabled={isDisabled}
            options={Questions}
          />
        </Grid>

        {!isDisabled && <InfoBox message={t("If you do not have access to the appropriate transportation or accommodations, these may be provided to you")} />}
      </Grid>
    </Card>
  );
};
