import { IsString, Length, IsOptional } from "class-validator";

export class ActivityDTO {
    @IsString()
    @Length(1,128)
    status: string;

    @IsString()
    @Length(0,2048)
    @IsOptional()
    note: string;
}
