import { Injectable, HttpException, HttpStatus } from "@nestjs/common";
import { ServiceAlbertaFormRO } from "./ro/form.ro";
import { DbService } from '../db/dbservice';
import { ActivityRO } from "./ro/activity.ro";
import { AgentRO } from "./ro/agent.ro";

export interface AgentDTO {
  id: string;
  name: string;
}

@Injectable()
export class ServiceAlbertaRepository {
  async getOrCreateAgent(user: any): Promise<AgentDTO> {
    const res = await DbService.getAgent(user);

    const [agent] = res || [null];

    if (agent) {
      return {
        id: agent.ID,
        name: agent.NAME,
      };
    } else {
      const res = await DbService.createAgent(user);

      const [{ ID = null }] = res || [];

      return {
        id: ID,
        name: user.name,
      };
    }
    
  }

  /**
   * Lists agents that have submitted at least one form activity
   */
  async listActiveAgents(): Promise<AgentDTO[]> {
    return await DbService.listActiveAgents();
  }

  /**
   * Find a record by confirmation number
   *
   * @param confirmationNumber
   *
   * @returns - Form fields (as needed by form) with dateOfBirth replaced with Minor/Adult
   */
  async findByConfirmationNumber(
    confirmationNumber: string
  ): Promise<ServiceAlbertaFormRO> {
    const res = await (
      await DbService.getServiceFormByNumber(confirmationNumber)
    ).singleService();

    res.activities = await this.getFormActivity(res.id);

    return res;
  }

  /**
   * Find an activity by id
   *
   * @param formId - id of a form associated with this activity
   *
   * @returns - activity details
   */
  async getFormActivity(formId: number): Promise<any> {
    const res = await DbService.getActivity(formId);

    if(!res) {
      return [];
    }

    return res.map(r => new ActivityRO({
      date: r.DATE,
      agent: r.AGENTNAME,
      type: r.TYPE,
      note: r.NOTE,
      status: r.STATUS
    }));
  }

  async assignTo(arrivalFormId: number, user: any, forceAssign?: boolean) {
    // Check if form has already been assigned
    const assignedAgent = await this.assignedTo(arrivalFormId);

    if (!forceAssign) {
      if (assignedAgent && assignedAgent.id !== user.sub) {
        // Throw en error if record is assigned to someone else
        throw new HttpException(
          `Record is already assigned to ${assignedAgent.name}`,
          HttpStatus.BAD_REQUEST
        );
      } else if (assignedAgent && assignedAgent.id === user.sub) {
        // Otherwise no need to re-assigbn it
        return;
      }
    }

    const agent = await this.getOrCreateAgent(user);

    await DbService.assignTo(agent.id, assignedAgent?.id || null, arrivalFormId);
  }

  /**
   * unassign the current user if they are assigned to it
   *
   * @param arrivalFormId - form id
   * @param user - Service Alberta agent id
   */
  async unassignFrom(arrivalFormId: number, user: any) {
    const assignedAgent = await this.assignedTo(arrivalFormId);
    if (!assignedAgent) {
      // Throw an error if record is assigned to someone else
      throw new HttpException(
        `There is no agent assigned to that record`,
        HttpStatus.BAD_REQUEST
      );
    }

    await DbService.assignTo(null, assignedAgent.id, arrivalFormId);
  }

  private async assignedTo(
    arrivalFormId: number
  ): Promise<AgentDTO> {
    const res = await DbService.assignedTo(arrivalFormId);

    const [agent] = res || [null];

    return agent && agent !== []
      ? {
          id: agent.ID,
          name: agent.NAME,
        }
      : null;
  }
}
