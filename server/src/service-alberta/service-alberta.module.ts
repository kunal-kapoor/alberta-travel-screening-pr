import { ServiceAlbertaAdminController } from "./service-alberta.controller";
import { ACLController } from "./acl.controller";
import { Module } from "@nestjs/common";
import { ServiceAlbertaRepository } from "./service-alberta.repository";
import { ArrivalFormRepository } from "../arrival-form/repositories/arrival-form.repository";
import { ServiceAlbertaService } from "./service-alberta.service";
import { ACLRepository } from "./acl.repository";
import { DailyReminderRepository } from "../rtop-admin/repositories/daily-reminder.repository.";
import { DailyQuestionnaireService } from "../rtop-admin/daily-reminder.service";
import { TextService } from "../text.service";
import { EmailService } from "../email/email.service";

@Module({
    providers: [
        ServiceAlbertaRepository,
        ArrivalFormRepository,
        ServiceAlbertaService,
        ACLRepository,
        DailyReminderRepository,
        DailyQuestionnaireService,
        EmailService,
        TextService
    ],
    imports: [],
    controllers: [ServiceAlbertaAdminController, ACLController]
  })
  export class ServiceAlbertaModule {}
  