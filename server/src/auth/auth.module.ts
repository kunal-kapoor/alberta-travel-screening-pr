import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { GOAPassportStrategy } from './goa/goa.api.strategy';
import { AHSPassportStrategy } from './ahs/ahs.api.strategy';
import { OthersPassportStrategy } from './others/others.api.stategy';
import { JwtStrategy } from './b2b-api-jwt-strategy';
@Module({
  imports: [
    PassportModule,
  ],
  providers: [GOAPassportStrategy,AHSPassportStrategy,OthersPassportStrategy, JwtStrategy],
  exports: [PassportModule],
})
export class AuthModule {}
