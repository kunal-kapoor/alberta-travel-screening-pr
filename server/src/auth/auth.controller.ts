import { Controller, Get, UseGuards, Logger, Req } from '@nestjs/common';
import { APIAuthGuard } from './api.guard';
import { LoggerService } from '../logs/logger';
import { ActionName } from '../logs/enums';
import { Roles } from '../decorators';

@Controller('api/v1/auth')
export class AuthController {
    /**
     * Returns ok if auth token is successfully validated
     */
    @Roles('AHS','GOA','OTHER')
    @UseGuards(APIAuthGuard)
    @Get('/validate')
    async validate(@Req() req):Promise<string>{
        LoggerService.logData(req, ActionName.VALIDATE_TOKEN, null, []);
        return 'ok';
    }
}
