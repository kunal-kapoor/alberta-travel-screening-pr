import { NestFactory } from "@nestjs/core";
import { AppModule } from "../../app.module";
import { ArrivalFormSeedService } from "../../arrival-form/arrival-form-seed.service";
import { LoggerService } from "../../logs/logger";

/**
 * Seeds the database with entries for testing purposes.
 */

const node_env = process.env.NODE_ENV;
(async () => {
  if(node_env !== 'development' && node_env !== 'test') {
    LoggerService.info('Aborting seeding. NODE_ENV not development or test.')
    // Sanity check to make sure we only seed dev and test databases
    return;
  }

  const appContext = await NestFactory.createApplicationContext(AppModule)
  
  try {
    const seeder = await appContext.get(ArrivalFormSeedService);
    const numOfRecords = process.env.npm_config_records || 25;

    await (await seeder.seedBuilder(+numOfRecords)).create();
    LoggerService.info('Seeding of arrival forms complete');
  } catch (e){
    LoggerService.info('Failed to seed arrival forms');
    throw e;
  } finally {
    appContext.close();
  }
})();

module.exports = {};
