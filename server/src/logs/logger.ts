import { Injectable } from '@nestjs/common';
import * as winston from 'winston';
import 'winston-daily-rotate-file';
import {ActionName} from './enums';
import { NestJSConsoleFormatter } from './nestjswinstonformatter';

@Injectable()
export class LoggerService {
  public static logger = winston.createLogger({
    transports: [
      new winston.transports.Console({
        format: NestJSConsoleFormatter,
        level: 'info',
      }),
      new winston.transports.DailyRotateFile({
        filename: 'alberta-health-%DATE%.log',
        dirname: '/logs',
        datePattern: 'YYYY-MM-DD',
        zippedArchive: true,
        level: 'info',
        maxFiles: '3650d'   // keep logs for 10 years
      }),
    ]
  });

  /**
   * Logs any errors occurred on the server
   *
   * @param message - Error message
   */
  public static async error(message: string, error?) {

    if(error && error.message) {
      message = `${message}: ${error.message}`;
    }

    if(error && error.sqlcode) {
      message = `${message}: ${error.sqlcode}`;
    }

    try {
      const log = `ERR: [${new Date().toUTCString()}] - [${message}] - ACTION: [DEBUG]\n`;
      await LoggerService.logger.error(log);
    } catch (err) {
      LoggerService.logger.error(err);
    }
  }

  /**
   * Logs any transaction performed on the server
   *
   * @remarks
   * NOTE: PII is not stored in the logs!
   *
   * @param message - Log data
   */
  public static async info(message: string) {
    try {
      const log = `MSG: [${new Date().toUTCString()}] - [${message}] - ACTION: [INFO]\n`;
      await LoggerService.logger.info(log);
    } catch (err) {
      LoggerService.logger.error(err);
    }
  }

  /**
   * Logs every API transaction that results in data access
   *
   * @remarks
   * NOTE: Do not pass PII into <input>
   * @param req - Nestjs request for the current application context
   * @param actionName - refers to the actions performed by the user during an access
   * @param facilityName - refers to the name of the facility/organization (legal entity) at which an access is performed (uses the Wellnet Distributed Facility Identifier (WDFA))
   * @param input - parameters being passed to a query
   * @param results - results of a query
   */
  public static async logData(req, actionName: ActionName, input: string, results: string[] | number[]) {
    req = req || {};
    
    try {
      const userId = !req.user? '<unauthicated>': req.user.sub || req.user.email || req.user.name || 'UNKNOWN USER';
      const facilityName = !req.user? '': JSON.stringify(req.user.identities || []);

      input = input || '';

      const resultStr = (results || []).join(',');

      const log = `[${new Date().toUTCString()}] - USER[${userId}] - FACILITY[${facilityName}]  - ACTION[${actionName}] - INPUT[${input}] - RESUTS[${resultStr}]\n`;
      await LoggerService.logger.info(log);
    } catch (err) {
      LoggerService.logger.error(err);
    }
  }
}