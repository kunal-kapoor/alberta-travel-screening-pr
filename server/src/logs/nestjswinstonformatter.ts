import * as winston from 'winston';
import colors from 'colors/safe';

/**
 * Formats the given console messages to resemble the built in default NestJS logger. 
 * 
 * Example:
 * info: 6/29/2020, 8:32:40 PM	 [RouterExplorer] Mapped {/api/v1/form, POST} route
 */
const consoleFormat = winston.format.printf(({ context, level, timestamp, message }) => {
    return `${level}: ${new Date(timestamp).toLocaleString()}\t [${colors.yellow(context)}] ${message}`;
});

/**
 * Use a colorized version of the consoleFormat
 * with timestamp added.
 */
export const NestJSConsoleFormatter = winston.format.combine(
    winston.format.timestamp(),
    winston.format.colorize({ all: true }),
    consoleFormat
);
