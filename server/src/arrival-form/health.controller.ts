import { Get, Controller } from "@nestjs/common";
import { HealthCheck, HealthCheckResult } from "@nestjs/terminus";
import { PublicRoute } from "../decorators";

@Controller('/api/v1/health')
export class HealthController {
  @Get()
  @HealthCheck()
  @PublicRoute()
  checkHealth(): HealthCheckResult {
    return <HealthCheckResult> {
      status: "ok",
      info: {
        app: {
          status: "up"
        }
      },
      error: {},
      details: {
        app: {
          status: "up"
        }
      }
    };  
  }
}
