export class QuarantineLocation {
  id: number;
  address: string;
  cityOrTown: string;
  provinceTerritory: string;
  postalCode: string;
  phoneNumber: string;
  typeOfPlace: string;
  howToGetToPlace: string;
  doesVulnerablePersonLiveThere: boolean;
  otherPeopleResiding: boolean;
}
