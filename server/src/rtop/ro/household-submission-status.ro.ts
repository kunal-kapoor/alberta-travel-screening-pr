import { EnrollmentStatus } from "../entities/household.entity";
import { Exclude, Expose } from "class-transformer";
import moment from "moment";
import { DailySubmissionAnswer } from "../enrollment-form.constants";
import { DAILY_QUESTIONS } from "../constants";
import { DailyQuestionAnswerService } from "../daily-question-answer.service";
import { ReminderIDRO } from "./reminder-id.ro";

export class DailyStatusRO {
    status: string;
    date: Date;
    submission: DailySubmissionAnswer[];

    constructor(data: any, reminder: ReminderIDRO) {
        this.status = data.status;
        this.date = data.date;

        const answers = data.submission && JSON.parse(data.submission)['answers']

        if(answers) {
            this.submission = DailyQuestionAnswerService.decodeAnswers(answers, reminder);
        }
    }
}

@Exclude()
export class HouseholdTravellersSubmissionStatusRO {

    @Expose()
    firstName: string;
    @Expose()
    lastName: string;
    @Expose()
    confirmationNumber: string;
    @Expose()
    age: string;
    @Expose()
    cardStatus: EnrollmentStatus;
    @Expose()
    status: DailyStatusRO[];
    @Expose()
    lastUpdated: string;
    @Expose()
    isolationStatus: boolean;

    constructor(data: any) {
        this.firstName = data['FIRST_NAME'];
        this.lastName = data['LAST_NAME'];
        this.confirmationNumber = data['CONFIRMATION_NUMBER'];
        this.age = (moment(new Date(), "YYYY/MM/DD").diff(moment(data['BIRTH_DATE'], "YYYY/MM/DD"), 'years') <= 18) ? "Minor" : "Adult";
        this.cardStatus = data['CARD_STATUS'];
        this.status = data['STATUS'];
        this.lastUpdated = data['CARD_STATUS_TIME'] && moment(data['CARD_STATUS_TIME']).format("DD MMM YYYY");
        this.isolationStatus = data['ISOLATION_STATUS'];
    }
}

@Exclude()
export class HouseholdTravellersQueryStatusRO {
    @Expose()
    results: HouseholdTravellersSubmissionStatusRO[];

    constructor(results: Array<any>) {
        const confirmationNumberObj: { [key: string]: Array<any> } = {};
        results.forEach((element) => {
            if (!(element['CONFIRMATION_NUMBER'] in confirmationNumberObj)) {
                confirmationNumberObj[element['CONFIRMATION_NUMBER']] = [];
            }
            confirmationNumberObj[element['CONFIRMATION_NUMBER']].push(element);
        });

        this.results = Object.entries(confirmationNumberObj).map(([key, value]) => {
            const element = {};
            Object.assign(element, value[0]);

            let submissionStatus = value.map(ele => {
                const reminder = new ReminderIDRO(ele, ele['TOKEN']);

                return ele['STATUS'] && new DailyStatusRO({ status: ele['STATUS'], date: ele["DATE"], submission: ele["SUBMISSION"] }, reminder);
            });
            submissionStatus = submissionStatus.filter((submission) => submission);
            element['STATUS'] = submissionStatus.length > 0 && submissionStatus;


            return new HouseholdTravellersSubmissionStatusRO(element);
        });
    }
}
