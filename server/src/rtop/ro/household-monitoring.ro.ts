import { Exclude, Expose } from "class-transformer";
import { ValidateNested } from "class-validator";
import moment from "moment";
import { RTOPActivityRO } from "./activity.ro";
import { EnrollmentStatus } from "../entities/household.entity";
import { HouseholdTravellersSubmissionStatusRO } from "./household-submission-status.ro";
import { EnrollmentFormRO } from "./enrollment-form.ro";

@Exclude()
export class HouseholdMonitoringRO {
    @Expose()
    id: number;
    @Expose()
    firstName: string;
    @Expose()
    lastName: string;

    @Expose()
    confirmationNumber: string;
  
    @Expose()
    agentId?: string;
    
    @Expose()
    agent: string;
  
    @Expose()
    age: string;
  
    @Expose()
    phoneNumber: string;
    
    @Expose()
    email: string;

    @Expose()
    contactMethod: string;

    @Expose()
    arrivalDate: string;
  
    @Expose()
    @ValidateNested({each: true})
    activities: Array<RTOPActivityRO>;

    @Expose()
    householdCardStatus: string;
    
    @Expose()
    @ValidateNested({each: true})
    travellerStatus: HouseholdTravellersSubmissionStatusRO[];
    
    @Expose()
    dailyReminderLink: string;

    @Expose()
    dailyReminderTravellerLink?: string;

    @Expose()
    assignedToMe: boolean;
  
    @Expose()
    assignedTo: string;

    @Expose()
    enrollmentStatus: EnrollmentStatus;
    
    @Expose()
    household: EnrollmentFormRO

    constructor(data) {
      data.age = (moment(new Date(), "YYYY/MM/DD").diff(moment(data.dateOfBirth, "YYYY/MM/DD"), 'years') <= 18)? "Minor": "Adult";
      Object.assign(this, data);
      this.household = new EnrollmentFormRO(data);
      this.activities = data.activities? data.activities.map(t => new RTOPActivityRO(t)): [];
    }
  
}