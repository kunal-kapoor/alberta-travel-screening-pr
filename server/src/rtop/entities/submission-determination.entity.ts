export class SubmissionDetermination {
  id: number;
  created: Date;
  lastUpdated: Date;
  notes: string;
  determination: string;
  exempt: boolean;
}
