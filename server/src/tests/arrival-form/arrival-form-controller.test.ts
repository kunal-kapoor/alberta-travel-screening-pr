import request from 'supertest';
import testData from '../../db/seed-data/arrival-form-seed-data.json'
import { createTestingApp } from '../util/db-test-utils';
import { IsolationPlanReportRepository } from '../../arrival-form/repositories/isolation-plan.repository';
import moment from 'moment';
import { DeterminationDecision } from '../../arrival-form/arrival-form.service';
import { IsolationPlanReport } from '../../arrival-form/entities/isolation-plan-report.entity';
import { envCond } from "../util/test-utils";


describe('Insert form into the databse. ', () => {
    let app;
    let isolationPlanReportProperty: IsolationPlanReportRepository;

    beforeEach(async () => {
        app = await createTestingApp();
        const context = await app.start();
        isolationPlanReportProperty = context.get(IsolationPlanReportRepository);
    });

    afterEach(async () => {
        await app.close();
    });

    envCond('TRAVELLER')('Should successfully insert a form to the database', async () => {
        const tempForm = Object.assign({}, testData.forms[0]);
        delete (tempForm.confirmationNumber);
        const res = await request(app.server())
            .post('/api/v1/form')
            .send(tempForm)
            .expect(201);

        expect(res.body.confirmationNumber).toBeDefined();
    });

    envCond('TRAVELLER')('After creating the form, a valid confirmation number is returned', async () => {
        const tempForm = Object.assign({}, testData.forms[0]);
        delete (tempForm.confirmationNumber);
        const res = await request(app.server())
            .post('/api/v1/form')
            .send(tempForm)

        expect(res.body.confirmationNumber).toBeDefined();

        await request(app.server())
            .get('/api/v1/admin/back-office/form/' + res.body.confirmationNumber)
            .expect(200)
    });

    envCond('TRAVELLER')('Unicode characters should be represented properly when retrieved', async () => {
        // Random set of unicode chars (https://www.w3.org/2001/06/utf-8-test/UTF-8-demo.html)
        const charString = 'IñtërnâtiônàližætiønĀdamΣὲγνωρίζωแผ่นดินฮั่นเสื่อሰማይአይታΑΒΓΔΩαβγδωრეგისტრაციაАБВГДабвгдコンニチハ';
        const tempForm = Object.assign({}, testData.forms[0]);
        delete (tempForm.confirmationNumber);
        tempForm.firstName = charString;
        const postRes = await request(app.server())
            .post('/api/v1/form')
            .send(tempForm)
            .expect(201);

        const confirmationNumber = postRes.body.confirmationNumber;

        const insertedEntity = await request(app.server())
            .get(`/api/v1/admin/back-office/form/${confirmationNumber}`)
            .expect(200)

        expect(insertedEntity.body.firstName).toBe(charString);
    });

    envCond('TRAVELLER')('"Boolean" fields should return yes when passed as yes', async () => {
        const tempForm = Object.assign({}, testData.forms[0]);
        tempForm.hasAdditionalTravellers = "Yes";
        const res = await request(app.server())
            .post('/api/v1/form')
            .send(tempForm);

        const {body} = await request(app.server())
            .get('/api/v1/admin/back-office/form/' + res.body.confirmationNumber);

        expect(body.hasAdditionalTravellers).toBe("Yes");
    });

    envCond('TRAVELLER')('"Boolean" fields should return no when passed as no', async () => {
        const tempForm = Object.assign({}, testData.forms[0]);
        tempForm.hasAdditionalTravellers = "No";
        const res = await request(app.server())
            .post('/api/v1/form')
            .send(tempForm);

        const {body} = await request(app.server())
            .get('/api/v1/admin/back-office/form/' + res.body.confirmationNumber);

            expect(body.hasAdditionalTravellers).toBe("No");
    });

    envCond('TRAVELLER')('Inputting 10 additional travellers submits successfully', async () => {
        const tempForm = testData.forms[0];
        tempForm.hasAdditionalTravellers = 'Yes';
        tempForm.additionalTravellers = Array(10).fill(0).map(() => ({
            firstName: 'John',
            lastName: 'Smith',
            dateOfBirth: '2012/01/01'
        }));

    });
    envCond('TRAVELLER')('Inputting 11 additional travellers fails', async () => {
        const tempForm = Object.assign({}, testData.forms[0]);
        tempForm.hasAdditionalTravellers = 'Yes';
        tempForm.additionalTravellers = Array(11).fill(0).map(() => ({
            firstName: 'John',
            lastName: 'Smith',
            dateOfBirth: '2012/01/01'
        }));

        await request(app.server())
            .post('/api/v1/form')
            .send(tempForm)
            .expect(400);
    });

    envCond('TRAVELLER')('Inputting 10 additional countries submits successfully', async () => {
        const tempForm = Object.assign({}, testData.forms[0]);

        tempForm.additionalCitiesAndCountries = Array(10).fill(0).map(() => ({
            cityOrTown: 'Victoria',
            country: 'Canada'
        }));

    });

    envCond('TRAVELLER')('Inputting 11 additional countries fails', async () => {
        const tempForm = Object.assign({}, testData.forms[0]);

        tempForm.additionalCitiesAndCountries = Array(11).fill(0).map(() => ({
            cityOrTown: 'Victoria',
            country: 'Canada'
        }));

    });

    envCond('TRAVELLER')('Inputting today as date of birth succeeds', async () => {
        const tempForm = Object.assign({}, testData.forms[0]);
        tempForm.dateOfBirth = moment().format('YYYY/MM/DD')
        await request(app.server())
            .post('/api/v1/form')
            .send(tempForm)
            .expect(201)

    });

    envCond('TRAVELLER')('Inputting yesterday as date of birth succeeds', async () => {
        const tempForm = Object.assign({}, testData.forms[0]);
        tempForm.dateOfBirth = moment().subtract(1, 'days').format('YYYY/MM/DD')
        await request(app.server())
            .post('/api/v1/form')
            .send(tempForm)
            .expect(201);

    });

    envCond('TRAVELLER')('Inputting a future date of birth returns 400', async () => {
        const tempForm = Object.assign({}, testData.forms[0]);
        tempForm.dateOfBirth = moment().add(1, 'days').format('YYYY/MM/DD')
        await request(app.server())
            .post('/api/v1/form')
            .send(tempForm)
            .expect(400);
    });

    envCond('TRAVELLER')('Inputting today as arrival date succeeds', async () => {
        const tempForm = Object.assign({}, testData.forms[0]);
        tempForm.arrivalDate = moment().format('YYYY/MM/DD')
        await request(app.server())
            .post('/api/v1/form')
            .send(tempForm)
            .expect(201)

    });

    envCond('TRAVELLER')('Inputting a future arrival date succeedes', async () => {
        const tempForm = Object.assign({}, testData.forms[0]);
        tempForm.arrivalDate = moment().add(1, 'days').format('YYYY/MM/DD')
        await request(app.server())
            .post('/api/v1/form')
            .send(tempForm)
            .expect(201);
    });

    envCond('TRAVELLER')('Inputting today as traveller dob succeeds', async () => {
        const tempForm = Object.assign({}, testData.forms[0]);
        tempForm.hasAdditionalTravellers = 'Yes';
        tempForm.additionalTravellers = [
           {
               dateOfBirth: moment().format('YYYY/MM/DD'),
               firstName: 'Test',
               lastName: 'Traveller'
           }
        ];

        await request(app.server())
            .post('/api/v1/form')
            .send(tempForm)
            .expect(201)

    });

    envCond('TRAVELLER')('Inputting a future traveller dob returns 400', async () => {
        const tempForm = Object.assign({}, testData.forms[0]);
        tempForm.hasAdditionalTravellers = 'Yes';
        tempForm.additionalTravellers = [
           {
               dateOfBirth: moment().add(1, 'days').format('YYYY/MM/DD'),
               firstName: 'Test',
               lastName: 'Traveller'
           }
        ];
        await request(app.server())
            .post('/api/v1/form')
            .send(tempForm)
            .expect(400);
    });

    describe('reports', () => {
        envCond('TRAVELLER')('Reports are empty when no forms have been submitted', async() => {
            expect(await isolationPlanReportProperty.count()).toEqual(0);
        });

        const dateString =  moment().format('YYYY/MM/DD');

        const postFormAndGetReport = async(form) => {
            delete (form.confirmationNumber);

            await request(app.server())
                .post('/api/v1/form')
                .send(form)
                .expect(201);

            expect(await isolationPlanReportProperty.count()).toEqual(1);
            return await isolationPlanReportProperty.findByDate(dateString);
        }

        const postFormAndDetermination = async (form, determination) => {
            delete form.confirmationNumber;
            await request(app.server())
                .post('/api/v1/form')
                .send(form)
                .expect(201);

            await request(app.server())
                .patch('/api/v1/admin/back-office/form/1/determination')
                .send(determination)
                .expect(200);
            expect(await isolationPlanReportProperty.count()).toEqual(1);
            return await isolationPlanReportProperty.findByDate(dateString);
        }

        envCond('TRAVELLER')('Increments failedThenPassed when failing vulnerable and passing determination', async () => {
            const tempForm = Object.assign({}, testData.forms[0]);
            tempForm.hasPlaceToStayForQuarantine = 'Yes';
            tempForm.quarantineLocation.doesVulnerablePersonLiveThere = 'Yes';
            tempForm.quarantineLocation.otherPeopleResiding = 'No';

            const determination = {
                determination: DeterminationDecision.ACCEPTED,
                notes: 'Yayy'
            };

            const report = await postFormAndDetermination(tempForm, determination);

            expect(report).toEqual({
                id: 1,
                date: dateString,
                failed: 1,
                passed: 0,
                failedThenPassed: 1,
                passedThenFailed: 0
            })
        });

        envCond('TRAVELLER')('Increments passedThenFailed when passing isolation check and failing determination', async () => {
            const tempForm = Object.assign({}, testData.forms[0]);
            tempForm.hasPlaceToStayForQuarantine = 'Yes';
            tempForm.quarantineLocation.doesVulnerablePersonLiveThere = 'No';
            tempForm.quarantineLocation.otherPeopleResiding = 'No';

            const determination = {
                determination: 'Not Accepted',
                notes: 'Yayy'
            };

            const report = await postFormAndDetermination(tempForm, determination);

            expect(report).toEqual({
                id: 1,
                date: dateString,
                failed: 0,
                passed: 1,
                failedThenPassed: 0,
                passedThenFailed: 1
            })
        });

        envCond('TRAVELLER')('Does not increment when passing isolation check and passing determination', async () => {
            const tempForm = Object.assign({}, testData.forms[0]);
            tempForm.hasPlaceToStayForQuarantine = 'Yes';
            tempForm.quarantineLocation.doesVulnerablePersonLiveThere = 'No';
            tempForm.quarantineLocation.otherPeopleResiding = 'No';

            const determination = {
                determination: DeterminationDecision.ACCEPTED,
                notes: 'Yayy'
            };

            const report = await postFormAndDetermination(tempForm, determination);

            expect(report).toEqual({
                id: 1,
                date: dateString,
                failed: 0,
                passed: 1,
                failedThenPassed: 0,
                passedThenFailed: 0
            })
        });

        envCond('TRAVELLER')('Increments passed report when passing isolation', async () => {
            const tempForm = Object.assign({}, testData.forms[0]);
            tempForm.hasPlaceToStayForQuarantine = 'Yes';
            tempForm.quarantineLocation.doesVulnerablePersonLiveThere = 'No';
            tempForm.quarantineLocation.otherPeopleResiding = 'No';
            const report = await postFormAndGetReport(tempForm);
            const expected:IsolationPlanReport = {
                id: 1,
                date: dateString,
                failed: 0,
                passed: 1,
                failedThenPassed: 0,
                passedThenFailed: 0
            }
            expect(report).toEqual(expected);
        });

        envCond('TRAVELLER')('Increments passed report when failing quarantine', async () => {
            const tempForm = Object.assign({}, testData.forms[0]);
            tempForm.hasPlaceToStayForQuarantine = 'No';
            tempForm.quarantineLocation.doesVulnerablePersonLiveThere = 'No';
            tempForm.quarantineLocation.otherPeopleResiding = 'No';

            const report = await postFormAndGetReport(tempForm);

            expect(report).toEqual({
                id: 1,
                date: dateString,
                failed: 1,
                passed: 0,
                failedThenPassed: 0,
                passedThenFailed: 0
            })
        });

        envCond('TRAVELLER')('Increments passed report when failing vulnerablePerson', async () => {
            const tempForm = Object.assign({}, testData.forms[0]);
            tempForm.hasPlaceToStayForQuarantine = 'Yes';
            tempForm.quarantineLocation.doesVulnerablePersonLiveThere = 'Yes';
            tempForm.quarantineLocation.otherPeopleResiding = 'No';

            const report = await postFormAndGetReport(tempForm);

            expect(report).toEqual({
                id: 1,
                date: dateString,
                failed: 1,
                passed: 0,
                failedThenPassed: 0,
                passedThenFailed: 0
            })
        });

        envCond('TRAVELLER')('Increments passed report when passing isolation twice', async () => {
            const tempForm = Object.assign({}, testData.forms[0]);
            tempForm.hasPlaceToStayForQuarantine = 'Yes';
            tempForm.quarantineLocation.doesVulnerablePersonLiveThere = 'No';
            tempForm.quarantineLocation.otherPeopleResiding = 'No';

            const report1 = await postFormAndGetReport(tempForm);

            expect(report1).toEqual({
                id: 1,
                date: dateString,
                failed: 0,
                passed: 1,
                failedThenPassed: 0,
                passedThenFailed: 0
            });

            const report22 = await postFormAndGetReport(tempForm);

            expect(report22).toEqual({
                id: 1,
                date: dateString,
                failed: 0,
                passed: 2,
                failedThenPassed: 0,
                passedThenFailed: 0
            });
        });
    });
});
