# Client
FROM node:12 AS client

# Build client
WORKDIR /client
COPY client/package*.json ./
RUN npm set progress=false && npm ci --no-cache
COPY client/. .
RUN npm run build:rtop-traveller

# Server
FROM node:12 AS server
ENV NODE_ENV production

# Run server
COPY --from=client /client/build /client/build/.
COPY --from=client /client/envsubst.sh /client/.
RUN chmod +x /client/envsubst.sh

WORKDIR /server
COPY server/. .

RUN npm set progress=false && npm ci --no-cache
RUN npm install -g typescript
RUN npm install -g copyfiles

RUN npm run build:traveller

RUN mkdir -p /logs

# Set to non-root user
RUN chown -R node:node /client
RUN chown -R node:node /server
RUN chown -R node:node /logs

USER node

EXPOSE 8080
CMD ["sh", "-c", "npm run start:traveller" ]
